#%%
"""
@author: Erialdo
"""
import pandas as pd
import numpy as np

data = pd.read_csv(data = pd.read_csv('Datasets/mainSimulationAccessTraces.csv')
data.describe()
#%%
'''1- PRÉ-PROCESSAMENTO '''

#Atribuindo o valor 'none' para os campos que tem o valor NaN
print(data.loc[pd.isnull(data['value'])])
data.loc[pd.isnull(data['value'])] = 'none'

#Foram removidos 106 registros que tinha o valor 'none' no campo value
data.drop(data[data.value == 'none'].index, inplace=True)

#Também foram removidos 25 966  registros que tinha o valor 'false' e 14 460 registros que tinham o valor 'true' no campo value
data.drop(data[data.value == 'false'].index, inplace=True)
data.drop(data[data.value == 'true'].index, inplace=True)
data.drop(data[data.value == 0].index, inplace=True)
data.drop(data[data.value == 1].index, inplace=True)

#Separando previsores da classe
previsores = data.iloc[:, 0:12].values
classe = data.iloc[:, 12].values

from sklearn.preprocessing import LabelEncoder, OneHotEncoder, label_binarize
from sklearn.compose import ColumnTransformer

#O mais correto seria utilizar o O.H.E, mas devido ao tamanho da base de dados utilizada ser muito grande, fica computacionalmente inviável utiizar o O.H.E
#Codificação dos previsores:
labelencoder_previsores = LabelEncoder()
for i in range(0, 11):
    previsores[:, i] = labelencoder_previsores.fit_transform(previsores[:, i])

# Adição de "features"(colunas) ruidosas para deixar o problema mais difícil
X = previsores
random_state = np.random.RandomState(0)
n_samples, n_features = X.shape
X = np.c_[X, random_state.randn(n_samples, 40 * n_features)]

#Deixando os valores dos previsores em um mesmo padrão numérico
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
previsores = scaler.fit_transform(previsores)

#Codificação da Classe:
labelencolder_classe = LabelEncoder()
classe = labelencolder_classe.fit_transform(classe)

#%%
'''2- TREINAMENTO DO MODELO (com GridSearch)'''
from sklearn.model_selection import train_test_split, GridSearchCV, StratifiedKFold
from sklearn.neural_network import MLPClassifier

previsores_treinamento, previsores_teste, classe_treinamento, classe_teste = train_test_split(previsores, classe, test_size=0.25, random_state=0)

classificador = MLPClassifier()

param_grid = { 'activation': ['softmax', 'tanh', 'relu'],
               'solver': ['adam'],
               'learning_rate': ['constant','adaptive'],
               'max_iter': [100]
              }

kFold = StratifiedKFold(n_splits=4, random_state=None)
mlp = GridSearchCV(classificador, param_grid, n_jobs=-1, cv=kFold)
mlp.fit(previsores_treinamento, classe_treinamento)

#%%
'''3- PREDIÇÕES E AVALIAÇÃO '''
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix, accuracy_score

previsoes = mlp.predict(previsores_teste)

rfc_cv_score = cross_val_score(classificador, previsores, classe, cv=kFold)

# report performance
print('Accuracy: %.3f (%.3f)' % (np.mean(rfc_cv_score), np.std(rfc_cv_score)))

precisao = accuracy_score(classe_teste, previsoes)
matriz = confusion_matrix(classe_teste, previsoes)

print("Matriz (sem 0s e 1s no campo value + ruido=40x\n\n)", matriz)
print("Precisão: ", precisao)
print("Previsões: ", previsoes)

'''PREDIÇÕES E AVALIAÇÃO----------------------------------------------------'''

# %%
'''4- DEMOSTRAÇÃO DOS RESULTADOS'''
from sklearn.metrics import roc_curve, roc_auc_score, auc
import matplotlib.pyplot as plt
from itertools import cycle
from scipy import interp

#ns_auc = roc_auc_score(classe_teste, ns_probs)
#rf_auc = roc_auc_score(classe_teste, rf_probs)
#y_score = mlp.decision_function(classe_teste)
classe_prob = mlp.predict_proba(previsores_teste)
print("classe_prob: ", classe_prob)
rf_auc = roc_auc_score(classe_teste, classe_prob, multi_class='ovr')
# = classificador.fit(previsores_treinamento, classe_treinamento).decision_function(previsores_treinamento)

# Binarização das saídas
classe_teste_bin = label_binarize(classe_teste, classes=[0, 1, 2, 3, 4, 5, 6, 7])
n_classes = classe_teste_bin.shape[1]
print(classe_teste_bin.shape[1])

# Binarização das predições de cada Classe
classe_prob = np.argmax(classe_prob, axis=1)
classe_prob = label_binarize(classe_prob, classes=[0, 1, 2, 3, 4, 5, 6, 7])
n_classes = classe_prob.shape[1]
print(classe_prob.shape[1])

# Computando uma curva e uma área ROC para cada classe
fpr = dict()
tpr = dict()
roc_auc = dict()
for i in range(n_classes):
    print("y_test_bin[:, i]:",classe_teste_bin[:, i])
    print("y_prob[:, i]", classe_prob[:, i])
    fpr[i], tpr[i], _ = roc_curve(classe_teste_bin[:, i], classe_prob[:, i])
    roc_auc[i] = auc(fpr[i], tpr[i])

#Computando curva e área ROC do micro-avarage 
fpr["micro"], tpr["micro"], _ = roc_curve(classe_teste_bin.ravel(), classe_prob.ravel())
roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

#%%
# Plotagem das curvas ROC:
# ..........................................
# Computando a curva e área ROC do macro-average
lw = 1.5
# Agregando todas as taxas de Falsos Positivos
all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))

# Interpolando todas as curvas ROC nesses pontos
mean_tpr = np.zeros_like(all_fpr)
for i in range(n_classes):
    mean_tpr += interp(all_fpr, fpr[i], tpr[i])

# Computando a média e a métrica AUC
mean_tpr /= n_classes

fpr["macro"] = all_fpr
tpr["macro"] = mean_tpr
roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

# Plotando todas as curvas ROC
plt.figure()
plt.plot(fpr["micro"], tpr["micro"],
         label='curva ROC do micro-average (area = {0:0.5f})'
               ''.format(roc_auc["micro"]),
         color='deeppink', linestyle=':', linewidth=4)

plt.plot(fpr["macro"], tpr["macro"],
         label='curva ROC do macro-average(area = {0:0.5f})'
               ''.format(roc_auc["macro"]),
         color='navy', linestyle=':', linewidth=4)

colors = cycle(['red', 'darkorange', 'cornflowerblue', 'green', 'blue', 'brown', 'magenta', 'aqua'])
for i, color in zip(range(n_classes), colors):
    plt.plot(fpr[i], tpr[i], color=color, lw=lw,
             label='curva ROC da classe {0} (area = {1:0.5f})'
             ''.format(i, roc_auc[i]))

plt.plot([0, 1], [0, 1], 'k--', lw=lw)
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Curvas ROC de cada classe')
plt.legend(loc="lower right")
plt.show()

#%%
# Área sob ROC para o problema multiclasse
''' .........................................
A função: func: `sklearn.metrics.roc_auc_score` pode ser usada para classificação multiclasse.
O esquema multiclasse Um contra Um compara cada combinação única de classes em pares.
Nesta seção, é calculdada a AUC usando os esquemas OvR e OvO.
É relatada uma média macro e uma média ponderada de prevalência.'''
y_prob = mlp.predict_proba(previsores_teste)

macro_roc_auc_ovo = roc_auc_score(classe_teste, y_prob, multi_class="ovo",
                                  average="macro")
weighted_roc_auc_ovo = roc_auc_score(classe_teste, y_prob, multi_class="ovo",
                                     average="weighted")
macro_roc_auc_ovr = roc_auc_score(classe_teste, y_prob, multi_class="ovr",
                                  average="macro")
weighted_roc_auc_ovr = roc_auc_score(classe_teste, y_prob, multi_class="ovr",
                                     average="weighted")
print("One-vs-One ROC AUC scores:\n{:.5f} (macro),\n{:.5f} "
      "(weighted by prevalence)"
      .format(macro_roc_auc_ovo, weighted_roc_auc_ovo))
print("One-vs-Rest ROC AUC scores:\n{:.5f} (macro),\n{:.5f} "
      "(weighted by prevalence)"
      .format(macro_roc_auc_ovr, weighted_roc_auc_ovr))

#%%
# Computando a pontuação de precisão média em configurações de vários rótulos
# ....................................................
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score

# Para cada classe
precision = dict()
recall = dict()
average_precision = dict()
for i in range(n_classes):
    precision[i], recall[i], _ = precision_recall_curve(classe_teste_bin[:, i],
                                                        classe_prob[:, i])
    average_precision[i] = average_precision_score(classe_teste_bin[:, i], classe_prob[:, i])

# A "micro-média": quantificação da pontuação em todas as classes em conjunto
precision["micro"], recall["micro"], _ = precision_recall_curve(classe_teste_bin.ravel(),
    classe_prob.ravel())
average_precision["micro"] = average_precision_score(classe_teste_bin, classe_prob,
                                                     average="micro")
print('Average precision score, micro-averaged over all classes: {0:0.4f}'
      .format(average_precision["micro"]))

# Plotando a curva de Precision-Recall da "micro-average"
# ...............................................

plt.figure()
plt.step(recall['micro'], precision['micro'], where='post')

plt.xlabel('Recall')
plt.ylabel('Precision')
plt.ylim([0.0, 1.05])
plt.xlim([0.0, 1.0])
plt.title(
    'Pontuação de precisão média, micro-média em todas as classes:AP={0:0.5f}'
    .format(average_precision["micro"]))

# Plotando a curva Precision-Recall para cada classe e curvas iso-f1
#.............................................................

from itertools import cycle
# setup plot details
colors = cycle(['red', 'darkorange', 'cornflowerblue', 'green', 'blue', 'brown', 'magenta', 'aqua'])

plt.figure(figsize=(7, 8))
f_scores = np.linspace(0.2, 0.8, num=4)
lines = []
labels = []
for f_score in f_scores:
    x = np.linspace(0.01, 1)
    y = f_score * x / (2 * x - f_score)
    l, = plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
    plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))

lines.append(l)
labels.append('iso-f1 curves')
l, = plt.plot(recall["micro"], precision["micro"], color='gold', lw=2)
lines.append(l)
labels.append('micro-average Precision-recall (area = {0:0.4f})'
              ''.format(average_precision["micro"]))


for i, color in zip(range(n_classes), colors):
    l, = plt.plot(recall[i], precision[i], color=color, lw=2)
    lines.append(l)
    labels.append('Precision-recall for class {0} (area = {1:0.5f})'
                  ''.format(i, average_precision[i]))

fig = plt.gcf()
fig.subplots_adjust(bottom=-0.25)
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.title('Extensão das curvas de Precision-Recall de cada classe')
plt.legend(lines, labels, loc=(0, -.38), prop=dict(size=14))

#%%
#Classification Report:
from sklearn.metrics import classification_report
from sklearn import metrics
import numpy as np

classe_prob = np.argmax(classe_prob, axis=1)

print(classification_report(classe_teste, classe_prob))
classifRep = classification_report(classe_teste, classe_prob)

precision_score = metrics.precision_score(classe_teste, classe_prob, average='macro')
recall_score = metrics.recall_score(classe_teste, classe_prob, average='micro')
f1_score = metrics.f1_score(classe_teste, classe_prob, average='weighted')
fbeta_score = metrics.fbeta_score(classe_teste, classe_prob, average='macro', beta=0.5)

def save_classification_report(classe_teste, classe_prob):
    report = metrics.classification_report(classe_teste, classe_prob, output_dict=True)
    df_classification_report = pd.DataFrame(report).transpose()
    df_classification_report = df_classification_report.sort_values(by=['f1-score'], ascending=False)
    return df_classification_report

def save_matriz_confusao(matriz):
    df_matrizConf = pd.DataFrame(matriz).transpose()
    return df_matrizConf

cReport = save_classification_report(classe_teste, classe_prob)
cReport.to_csv('MLP_ClassifRep.csv')

matrizConf = save_matriz_confusao(matriz)
matrizConf.to_csv('MatrizConfusao.csv')

#%%
'''SALVANDO O MODELO DE APRENDIZADO'''
import pickle

with open('MLPClassifModel.pkf', 'wb') as file:
  pickle.dump(mlp, file)